package com.guillaumevdn.videomaker.part;

import java.util.List;

/**
 * @author GuillaumeVDN
 */
public class VideoPart {

	// base
	private List<VideoPartExtract> videoParts;

	public VideoPart(List<VideoPartExtract> videoParts) {
		this.videoParts = videoParts;
	}

	// get
	public List<VideoPartExtract> getVideoParts() {
		return videoParts;
	}

}
