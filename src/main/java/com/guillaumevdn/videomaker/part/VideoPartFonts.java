package com.guillaumevdn.videomaker.part;

import java.awt.Font;

/**
 * @author GuillaumeVDN
 */
public class VideoPartFonts {

	// base
	private int lengthFontA;
	private int lengthFontB;
	private int lengthFontC;
	private int textX;
	private int textYA;
	private int textYB;
	private int textYC;
	private Font fontA;
	private Font fontB;
	private Font fontC;

	public VideoPartFonts(int lengthFontA, int lengthFontB, int lengthFontC, int textX, int textYA, int textYB, int textYC, Font initialFont) {
		this.lengthFontA = lengthFontA;
		this.lengthFontB = lengthFontB;
		this.lengthFontC = lengthFontC;
		this.textX = textX;
		this.textYA = textYA;
		this.textYB = textYB;
		this.textYC = textYC;
		this.fontA = initialFont.deriveFont(initialFont.getStyle(), lengthFontA);
		this.fontB = initialFont.deriveFont(initialFont.getStyle(), lengthFontB);
		this.fontC = initialFont.deriveFont(initialFont.getStyle(), lengthFontC);
	}

	// get
	public int getLengthFontA() {
		return lengthFontA;
	}

	public int getLengthFontB() {
		return lengthFontB;
	}

	public int getLengthFontC() {
		return lengthFontC;
	}

	public int getTextX() {
		return textX;
	}

	public int getTextYA() {
		return textYA;
	}

	public int getTextYB() {
		return textYB;
	}

	public int getTextYC() {
		return textYC;
	}

	public Font getFontA() {
		return fontA;
	}

	public Font getFontB() {
		return fontB;
	}

	public Font getFontC() {
		return fontC;
	}

}
