package com.guillaumevdn.videomaker;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.guillaumevdn.videomaker.part.VideoPart;
import com.guillaumevdn.videomaker.part.VideoPartExtract;
import com.guillaumevdn.videomaker.part.VideoPartFonts;
import com.guillaumevdn.videomaker.util.Utils;

import net.bramp.ffmpeg.FFmpeg;

/**
 * @author GuillaumeVDN
 */
public class Main {

	public static void main(String[] args) {
		process(true, true, true, true);
	}

	public static boolean process(boolean mustExtract, boolean mustEdit, boolean mustTranscode, boolean mustMerge) {
		try {
			System.out.println(">> Initializing...");

			// settings
			File fileRoot = new File("/rootFile/");

			File fileResourcesFfmpeg = new File(fileRoot + "/resources/ffmpeg/bin/ffmpeg.exe");
			File fileResourcesFont = new File(fileRoot + "/resources/font.ttf");

			File fileEpisodesRoot = new File(fileRoot + "/episodes/");
			File fileVideoSettings = new File(fileRoot + "/video_settings.txt");

			File fileResultRoot = new File(fileRoot + "/result/");
			File fileResultExtractedRoot = new File(fileResultRoot + "/extracted/");
			File fileResultOverlaysRoot = new File(fileResultRoot + "/overlays/");
			File fileResultEditedRoot = new File(fileResultRoot + "/edited/");
			File fileResultTranscodedRoot = new File(fileResultRoot + "/transcoded/");
			File fileResultVideo = new File(fileResultRoot + "/result.mp4");

			// init 
			FFmpeg ffmpeg = new FFmpeg(fileResourcesFfmpeg.getAbsolutePath());

			Font font = Font.createFont(Font.TRUETYPE_FONT, fileResourcesFont);
			if (!GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font)) {
				throw new Error("couldn't register font");
			}

			VideoPartFonts fonts720 = new VideoPartFonts(17, 35, 12, 75, 620, 665, 687, font);
			VideoPartFonts fonts1080 = new VideoPartFonts(25, 50, 20, 50, 935, 995, 1030, font);

			// init files
			fileResultRoot.mkdirs();

			if (mustExtract) {
				Utils.deleteFile(fileResultExtractedRoot);
				fileResultExtractedRoot.mkdirs();
			}
			if (mustEdit) {
				Utils.deleteFile(fileResultEditedRoot);
				Utils.deleteFile(fileResultOverlaysRoot);
				fileResultEditedRoot.mkdirs();
				fileResultOverlaysRoot.mkdirs();
			}
			if (mustTranscode) {
				Utils.deleteFile(fileResultTranscodedRoot);
				fileResultTranscodedRoot.mkdirs();
			}
			if (mustMerge) {
				Utils.deleteFile(fileResultVideo);
			}

			// read raw extract parts
			List<List<Object[]>> rawParts = new ArrayList<>();
			int extractCount = 0;
			try (BufferedReader reader = new BufferedReader(new FileReader(fileVideoSettings))) {
				int lineNb = 0;
				String line;
				while ((line = reader.readLine()) != null) {
					// read settings
					++lineNb;
					if (line.trim().isEmpty()) continue;
					if (line.trim().equalsIgnoreCase("stop")) break;
					if (line.trim().equalsIgnoreCase("reset")) {
						rawParts.clear();
						continue;
					}
					String[] split = line.trim().split(" ");
					if (split.length < 3 || split.length % 2 == 0) {
						throw new IllegalArgumentException("Invalid settings at line " + lineNb + " '" + line + "'");
					}
					// read episode
					int episode = Integer.parseInt(split[0]);
					String episodeName = "Épisode " + episode;
					File episodeFile = new File(fileEpisodesRoot + "/" + String.valueOf(episode) + ".mp4");
					if (!episodeFile.exists()) {
						throw new IllegalArgumentException("Invalid episode at line " + lineNb + " '" + line + "'");
					}
					// read extracts
					List<Object[]> rawExtract = new ArrayList<>();
					for (int i = 1; i < split.length; ++i) {
						rawExtract.add(new Object[] {
								++extractCount,
								split[i],
								split[++i],
								episodeName,
								episodeFile
						});
					}
					// add it to raw parts
					rawParts.add(rawExtract);
				}
			}

			// read raw parts
			int round = String.valueOf(extractCount).length();
			List<VideoPart> parts = new ArrayList<>();
			for (List<Object[]> rawPart : rawParts) {
				List<VideoPartExtract> partExtracts = new ArrayList<>();
				for (Object[] rawExtract : rawPart) {
					int extractId = (int) rawExtract[0];
					String startTime = (String) rawExtract[1];
					String endTime = (String) rawExtract[2];
					String episodeName = (String) rawExtract[3];
					File episodeFile = (File) rawExtract[4];
					String fileExtractId = Utils.formatNumber(extractId, round);
					File extractFile = new File(fileResultExtractedRoot + "/extract-" + fileExtractId + ".mp4");
					File overlayFile = new File(fileResultOverlaysRoot + "/overlay-" + fileExtractId + ".png");
					File editFile = new File(fileResultEditedRoot + "/edit-" + fileExtractId + ".mp4");
					File transcodeFile = new File(fileResultTranscodedRoot + "/transcode-" + fileExtractId + ".mp4");
					partExtracts.add(new VideoPartExtract(extractId, "Extrait " + extractId, episodeName, episodeFile, startTime, endTime, extractFile, overlayFile, editFile, transcodeFile));
				}
				parts.add(new VideoPart(partExtracts));
			}

			// extract parts
			if (mustExtract) {
				System.out.println(">> Extracting...");
				for (VideoPart part : parts) {
					for (VideoPartExtract extract : part.getVideoParts()) {
						System.out.println("	> Extracting '" + extract.getName() + "' from '" + extract.getEpisodeName() + "' (" + Utils.millisToReadableTimeString(extract.getEndMillis() - extract.getStartMillis()) + ")...");
						extract.extract(ffmpeg);
					}
				}
			}

			// edit parts
			if (mustEdit) {
				System.out.println(">> Editing...");
				for (VideoPart part : parts) {
					for (VideoPartExtract extract : part.getVideoParts()) {
						System.out.println("	> Editing '" + extract.getName() + "' from '" + extract.getEpisodeName() + "' (" + Utils.millisToReadableTimeString(extract.getEndMillis() - extract.getStartMillis()) + ")...");
						extract.edit(ffmpeg, fonts720, fonts1080);
					}
				}
			}

			// transcode parts
			if (mustTranscode) {
				System.out.println(">> Transcoding...");
				for (VideoPart part : parts) {
					for (VideoPartExtract extract : part.getVideoParts()) {
						System.out.println("	> Editing '" + extract.getName() + "' from '" + extract.getEpisodeName() + "' (" + Utils.millisToReadableTimeString(extract.getEndMillis() - extract.getStartMillis()) + ")...");
						extract.transcode(ffmpeg);
					}
				}
			}

			// merge parts
			if (mustMerge) {
				System.out.println(">> Merging...");
				String concat = "";
				for (VideoPart part : parts) {
					for (VideoPartExtract extract : part.getVideoParts()) {
						if (!concat.isEmpty()) concat += "|";
						concat += extract.getTranscodeFile().getAbsolutePath();
					}
				}
				Utils.runFFmpeg(ffmpeg,
						// input
						"-i", "\"concat:" + concat + "\"",
						// codec
						"-c", "copy",
						//"-bsf:a", "aac_adtstoasc",
						//"-f", "mpegts",
						// output
						fileResultVideo.getAbsolutePath(),
						// misc
						"-v", "error"
						);
			}

			// all done
			System.out.println(">> Done !");
			return true;
		} catch (Throwable exception) {
			exception.printStackTrace();
			return false;
		}
	}

}
